import os
import raven
from .base import *

DEBUG = False

SECRET_KEY = os.environ.get('TRAVEL_HEAVEN_SECRET_KEY', SECRET_KEY)
ALLOWED_HOSTS = os.environ.get('TRAVEL_HEAVEN_ALLOWED_HOSTS', '*').split(',')

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('TRAVEL_HEAVEN_DB_ENGINE', 'django.db.backends.postgresql'),
        'NAME': os.environ.get('TRAVEL_HEAVEN_DB_NAME', 'postgres'),
        'USER': os.environ.get('TRAVEL_HEAVEN_DB_USER', 'postgres'),
        'PASSWORD': os.environ.get('TRAVEL_HEAVEN_DB_PASS', ''),
        'HOST': os.environ.get('TRAVEL_HEAVEN_DB_HOST', '127.0.0.1'),
        'PORT': os.environ.get('TRAVEL_HEAVEN_DB_PORT', '5432'),
    }
}

ADMIN_URL = os.environ.get('TRAVEL_HEAVEN_ADMIN_URL', r'^admin/')

# S3 Storage configuration
DEFAULT_FILE_STORAGE = 'travelheaven.storages.S3Boto3MediaStorage'
STATICFILES_STORAGE = 'travelheaven.storages.S3Boto3StaticStorage'

AWS_ACCESS_KEY_ID = os.environ['TRAVEL_HEAVEN_AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['TRAVEL_HEAVEN_AWS_SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = 'travel-heaven'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_QUERYSTRING_AUTH = False
AWS_S3_SIGNATURE_VERSION = 's3v4'
AWS_S3_FILE_OVERWRITE = False

STATICFILES_LOCATION = 'static'
MEDIAFILES_LOCATION = 'media'
# Although not required, it is advised so that staticfiles.static won't break stuff
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)

# Sentry configuration
INSTALLED_APPS += ['raven.contrib.django.raven_compat']
RAVEN_CONFIG = {
    'dsn': os.environ['TRAVEL_HEAVEN_RAVEN_DSN'],
    'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
}
# Elasticsearch configuration
ELASTICSEARCH_HOST = os.environ.get('TRAVEL_HEAVEN_ELASTICSEARCH_HOST', '127.0.0.1')
QUERY_SEARCH_ENABLED = True if os.environ.get('TRAVEL_HEAVEN_SEARCH_ENABLED', None) else False
