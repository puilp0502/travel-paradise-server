from rest_framework import permissions
from rest_framework.compat import is_authenticated


class IsOwnerOrReadonlyPermissionMixin(permissions.BasePermission):
    owner_field = 'owner'  # A field representing owner of the object

    def has_object_permission(self, request, view, obj):
        owner = getattr(obj, self.owner_field)
        return (request.method in permissions.SAFE_METHODS or
                request.user and is_authenticated(request.user)
                and request.user == owner)
