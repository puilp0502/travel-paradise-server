import copy
from collections import OrderedDict
from pprint import pprint
from rest_framework import serializers
from rest_framework.reverse import reverse
from rest_framework_extensions.mixins import NestedViewSetMixin
from .models import (
    Trip,
    Log,
    Picture,
    Tag,
)
from accounts.serializers import UserSerializer


class TagSlugField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        try:
            obj, _ = self.get_queryset().get_or_create(**{self.slug_field: data})
            return obj
        except (TypeError, ValueError):
            self.fail('invalid')


class LogHyperlinkedRelatedField(serializers.HyperlinkedRelatedField):
    view_name = 'trips-log-detail'

    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'parent_lookup_trip': obj.trip.pk,
            'pk': obj.pk
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class PictureHyperlinkedRelatedField(serializers.HyperlinkedRelatedField):
    view_name = 'trips-logs-picture-detail'

    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'parent_lookup_trip': obj.log.trip.pk,
            'parent_lookup_log': obj.log.pk,
            'pk': obj.pk,
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


# class FileRelatedField(serializers.RelatedField):
#     """
#     Linear file url serialization (no dictionary, plain list)
#     """
#     def __init__(self, field=None, **kwargs):
#         self.field = field
#         super(FileRelatedField, self).__init__(**kwargs)
#
#     def to_representation(self, value):
#         request = self.context.get('request')
#         photo_url = getattr(value, self.field).url
#         return request.build_absolute_uri(photo_url)


class CurrentTrip(object):
    """
    Set default value to current trip
    """
    def set_context(self, serializer_field):
        self.trip = Trip.objects.get(pk=serializer_field.context['view'].kwargs['parent_lookup_trip'])

    def __call__(self):
        return self.trip

    def __repr__(self):
        return '%s()' % self.__class__.__name__


class LogSerializer(serializers.HyperlinkedModelSerializer):
    url = LogHyperlinkedRelatedField(
        read_only=True,
        source='*',
    )
    pictures = PictureHyperlinkedRelatedField(
        many=True,
        read_only=True
    )
    trip = serializers.HiddenField(
        default=CurrentTrip()
    )

    class Meta:
        model = Log
        fields = ('url', 'trip', 'description', 'pictures', 'location', 'location_name', 'location_address', 'visited_at')


def TripSerializerFactory(**kwargs):
    class TripSerializerBase(serializers.HyperlinkedModelSerializer):
        logs = LogSerializer(
            many=True,
            read_only=True,
        )
        writer = UserSerializer(read_only=True, default=serializers.CurrentUserDefault())
        tags = TagSlugField(
                many=True,
                slug_field='name',
                queryset=Tag.objects
            )

        class Meta:
            model = Trip
            fields = ('url', 'logs', 'name', 'writer', 'created_at', 'is_visible', "tags",)

    _base = copy.deepcopy(TripSerializerBase)

    try:
        additional_fields = kwargs['additional_fields']
        assert isinstance(additional_fields, dict), (
            'Additional fields must be dictionary object.'
        )
        attrs = _base.__dict__.copy()
        if serializers.ModelSerializer in _base.__mro__:
            fields = attrs['Meta'].fields + tuple(additional_fields.keys())
            attrs['Meta'].fields = tuple(fields)
        attrs['_declared_fields'].update(list(additional_fields.items()))
        # I dunno how this works, but it works so i'm using it
        return _base
    except KeyError:
        pass
    return _base

TripSerializer = TripSerializerFactory()
