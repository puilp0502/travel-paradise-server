from django.apps import AppConfig


class TravellogConfig(AppConfig):
    name = 'travellog'

    def ready(self):
        import travellog.search  # NOQA
