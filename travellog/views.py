from django.conf import settings
from django.db import IntegrityError
from django.db.models import Count
from django.shortcuts import redirect
from django.utils.datetime_safe import datetime
from rest_framework import exceptions
from rest_framework import status
from rest_framework import generics
from rest_framework import serializers
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotAuthenticated
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework_extensions.mixins import NestedViewSetMixin

from accounts.models import User
from accounts.serializers import UserSerializer
from .serializers import (
    TripSerializerFactory,
    TripSerializer,
    LogSerializer,
)
from .models import (
    Trip,
    Log,
    Picture,
    Tag,
    RecommendedTagPlan)
from .permissions import (
    IsLogOwnerOrReadOnly,
    IsTripOwnerOrReadOnly,
    IsPhotoOwnerOrReadOnly,
    IsTripVisibleOrNX,
    IsItselfVisibleOrNX
)
from .search import Trip as TripDocument
from travelheaven.pagination import TripPagination
from elasticsearch_dsl import Q


class LogViewSet(NestedViewSetMixin, ModelViewSet):
    permission_classes = (IsLogOwnerOrReadOnly, IsAuthenticatedOrReadOnly, IsTripVisibleOrNX)
    queryset = Log.objects.all()
    serializer_class = LogSerializer
    model = Log


class TripViewSet(NestedViewSetMixin, ModelViewSet):
    permission_classes = (IsTripOwnerOrReadOnly, IsAuthenticatedOrReadOnly, IsItselfVisibleOrNX)
    serializer_class = TripSerializerFactory(additional_fields={'likes': serializers.IntegerField(read_only=True)})
    model = Trip

    def get_queryset(self):
        return Trip.objects.annotate(likes=Count('liked_by'))


class LikeViewSet(ModelViewSet):
    """
    [GET]
    Lists users who liked the post.

    [POST]
    Like the post as a currently authenticated user.
    If not authenticated, 403 is returned.
    """
    pagination_class = TripPagination
    permission_classes = (IsAuthenticatedOrReadOnly, IsTripVisibleOrNX)
    serializer_class = UserSerializer

    def get_queryset(self):
        trip = Trip.objects.get(pk=self.kwargs['parent_lookup_trip'])
        return trip.liked_by.all()

    def create(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise NotAuthenticated
        trip = Trip.objects.get(pk=self.kwargs['parent_lookup_trip'])
        trip.liked_by.add(request.user)
        return Response({"message": "ok"})

    def destroy(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise NotAuthenticated
        trip = Trip.objects.get(pk=self.kwargs['parent_lookup_trip'])
        trip.liked_by.remove(request.user)
        return Response({"message": "ok"})


class PictureViewSet(ViewSet):
    parser_classes = (FormParser, MultiPartParser)
    permission_classes = (IsPhotoOwnerOrReadOnly, IsAuthenticatedOrReadOnly, IsTripVisibleOrNX)

    def create(self, request, **kwargs):
        try:
            log = Log.objects.get(trip=kwargs['parent_lookup_trip'],
                                  pk=kwargs['parent_lookup_log'])
        except Log.DoesNotExist:
            raise exceptions.NotFound()
        print(dir(request))
        print(request.FILES)
        for filename in request.FILES:
            p = Picture.objects.create(picture=request.FILES[filename], log=log)
            p.save()
        print(dir(self))
        return Response('ok')

    def list(self, request, **kwargs):
        try:
            log = Log.objects.get(trip=kwargs['parent_lookup_trip'],
                                  pk=kwargs['parent_lookup_log'])
        except Log.DoesNotExist:
            raise exceptions.NotFound()

        picture_list = []
        for picture in log.pictures.all():
            picture_list.append(request.build_absolute_uri(picture.picture.url))

        return Response(picture_list, status=status.HTTP_200_OK)

    def retrieve(self, request, **kwargs):
        try:
            log = Log.objects.get(trip=kwargs['parent_lookup_trip'],
                                  pk=kwargs['parent_lookup_log'])
            picture = Picture.objects.get(log=log, pk=kwargs['pk'])
        except (Picture.DoesNotExist, Log.DoesNotExist):
            raise exceptions.NotFound()

        return redirect(request.build_absolute_uri(picture.picture.url))

    def destroy(self, request, **kwargs):
        try:
            log = Log.objects.get(trip=kwargs['parent_lookup_trip'],
                                  pk=kwargs['parent_lookup_log'])
            picture = Picture.objects.get(log=log, pk=kwargs['pk'])
        except (Picture.DoesNotExist, Log.DoesNotExist):
            raise exceptions.NotFound()
        picture.picture.delete()
        picture.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TagView(APIView):
    def post(self, request):
        try:
            name = request.data['name']
            Tag.objects.create(name=name)
        except KeyError:
            return Response({'detail': 'Please provide a name.'}, status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError:
            return Response({'detail': 'Tag already exists.'}, status=status.HTTP_409_CONFLICT)
        return Response({'detail': 'OK'}, status=status.HTTP_201_CREATED)

    def get(self, request):
        try:
            startswith = request.query_params['startswith']
            qs = Tag.objects.filter(name__startswith=startswith)
        except KeyError:
            qs = Tag.objects.all()
        tags = [tag.name for tag in qs]
        return Response(tags)


class SearchByQueryView(APIView):
    def get(self, request):
        if not settings.QUERY_SEARCH_ENABLED:
            return Response({'detail': 'Query search feature is currently turned off.'},
                            status=status.HTTP_501_NOT_IMPLEMENTED)
        query = request.query_params.get('query', '')
        if query == '':
            return Response({'detail': 'Please provide a query.'}, status=status.HTTP_400_BAD_REQUEST)
        search = TripDocument.search()
        q = Q("query_string", fields=["title^5"], query=query)
        q |= Q("nested", path="logs", query=Q("query_string", fields=["logs.*"], query=query))
        f = Q("match", is_visible=True)
        hits = search.query(q).filter(f).sort('_score').execute()

        pks = [hit.meta.id for hit in hits]
        qs = Trip.objects.filter(pk__in=pks).prefetch_related('logs')

        unordered_list = TripSerializer(qs, many=True, context={'request': request}).data
        index = {}
        for (i, hit) in enumerate(hits):
            index[hit.title] = (i, hit._score)

        ordered_list = [None] * len(unordered_list)
        for trip in unordered_list:
            current_index = index[trip['name']]
            trip['score'] = current_index[1]
            ordered_list[current_index[0]] = trip

        return Response(ordered_list)


class SearchUserView(generics.ListAPIView):
    pagination_class = TripPagination
    serializer_class = UserSerializer

    def get_queryset(self):
        name = self.request.query_params.get('name', '')
        if name == '':
            raise exceptions.ParseError(detail='Please provide a username.')
        qs = User.objects.filter(display_name__contains=name)
        return qs


class SearchByTagView(generics.ListAPIView):
    pagination_class = TripPagination
    serializer_class = TripSerializer

    def get_queryset(self):
        tags = self.request.query_params.get('tags', '')
        if tags == '':
            raise exceptions.ParseError(detail='Please provide tag(s).')
        tags = tags.split(',')
        qs = Trip.objects.filter(tags__name__in=tags)\
            .annotate(t_count=Count('tags')).filter(t_count=len(tags)).filter(is_visible=True)
        return qs


@api_view(['GET'])
def get_recommended_tag(request):
    now = datetime.now()
    try:
        plan = RecommendedTagPlan.objects.filter(start__lte=now, end__gt=now)[0]
        background = request.build_absolute_uri(plan.background.url)
        tag = plan.tag
    except IndexError:
        tag = Tag.objects.all()[0]
        background = "http://2grv15ahqgzqolzl5aahh7jl.wpengine.netdna-cdn.com/wp-content/uploads/2015/06/late-night-singapore_river-cruise.jpg"

    return Response({"name": tag.name, "background": background})
