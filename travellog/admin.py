from django.contrib import admin

from .models import Trip, Log, Picture, Tag, RecommendedTagPlan

admin.site.register(Trip)
admin.site.register(Log)
admin.site.register(Picture)
admin.site.register(Tag)
admin.site.register(RecommendedTagPlan)
