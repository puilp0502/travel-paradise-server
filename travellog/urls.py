from django.conf.urls import url
from rest_framework_extensions.routers import ExtendedSimpleRouter
from .views import (
    LogViewSet,
    TripViewSet,
    PictureViewSet,
    LikeViewSet,
    SearchByQueryView,
    SearchUserView,
    SearchByTagView,
    TagView,
)

trip_router = ExtendedSimpleRouter()

root = trip_router.register(r'', TripViewSet, base_name='trip')

root.register(r'logs', LogViewSet, base_name='trips-log', parents_query_lookups=['trip'])\
      .register(r'pictures', PictureViewSet, base_name='trips-logs-picture', parents_query_lookups=['trip', 'log'])

root.register(r'likes', LikeViewSet, base_name='trips-like', parents_query_lookups=['trip'])

urlpatterns = [
    url(r'^search/query/', SearchByQueryView.as_view()),
    url(r'^search/user/', SearchUserView.as_view()),
    url(r'^search/tags/', SearchByTagView.as_view()),
    url(r'^tags/', TagView.as_view()),
] + trip_router.urls
