import json
import re
from datetime import datetime
from django.test import Client
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from accounts.models import User
from .models import (
    Trip,
    Log,
    Picture
)


class TravellogTripTestCase(TestCase):
    client_class = APIClient

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('test', 'password', picture='')
        cls.other_user = User.objects.create_user('test2', 'password', picture='')
        cls.trip = Trip.objects.create(writer=cls.user, name='My First Trip')
        cls.user_credential = {'uid': 'test', 'password': 'password'}
        cls.other_user_credential = {'uid': 'test2', 'password': 'password'}

    # Create
    def test_create_by_authorized(self):
        c = self.client
        c.login(**self.user_credential)
        resp = c.post(reverse('trip-list'), {'name': 'My Second Trip'})
        self.assertEqual(resp.status_code, 201, 'Response code should be 201\n'
                                                '<Response>\n'+resp.content.decode('utf-8'))

    def test_create_by_unauthorized(self):
        c = self.client
        resp = c.post(reverse('trip-list'), {'name': 'My Second Trip'})
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')

    # Retrieve
    def test_retrieve(self):
        c = self.client
        trip_list = json.loads(c.get(reverse('trip-list')).content.decode('utf-8'))
        self.assertEqual(Trip.objects.count(), trip_list['count'],
                         'Length of trip list should be equal to Trip object\'s count\n')

        trip_repr = trip_list['results'][0]
        pk = re.search(r'/(\d+)/$', trip_repr['url']).group(1)
        trip = Trip.objects.get(pk=pk)
        self.assertEqual(trip_repr['name'], trip.name)

    # Update
    def test_update_by_owner(self):
        c = self.client
        c.login(**self.user_credential)
        resp = c.put(reverse('trip-detail', kwargs={'pk': self.trip.pk}),
                     data={'name': 'New name', 'tags': []}, format='json')
        self.assertEqual(200, resp.status_code, 'Response code should be 200')
        trip = json.loads(c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk})).content.decode('utf-8'))
        self.assertEqual('New name', trip['name'])

    def test_update_by_others(self):
        c = self.client
        c.login(**self.other_user_credential)
        resp = c.put(reverse('trip-detail', kwargs={'pk': self.trip.pk}),
                     data={'name': 'New name'}, format='json')
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')
        trip = json.loads(c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk})).content.decode('utf-8'))
        self.assertEqual('My First Trip', trip['name'])  # Ensure it's not modified

    def test_update_by_unauthorized(self):
        c = self.client
        resp = c.put(reverse('trip-detail', kwargs={'pk': self.trip.pk}),
                     data={'name': 'New name'}, format='json')
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')
        trip = json.loads(c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk})).content.decode('utf-8'))
        self.assertEqual('My First Trip', trip['name'])  # Ensure it's not modified

    # Delete
    def test_delete_by_owner(self):
        c = self.client
        c.login(**self.user_credential)
        resp = c.delete(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertTrue(200 <= resp.status_code < 300, 'Response code should be 2xx')
        resp = c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertEqual(404, resp.status_code, 'Response code should be 404')

    def test_delete_by_others(self):
        c = self.client
        c.login(**self.other_user_credential)
        resp = c.delete(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')
        resp = c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertEqual(200, resp.status_code, 'Response code should be 200')  # Ensure it's not deleted

    def test_delete_by_unauthorized(self):
        c = self.client
        resp = c.delete(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')
        resp = c.get(reverse('trip-detail', kwargs={'pk': self.trip.pk}))
        self.assertEqual(200, resp.status_code, 'Response code should be 200')  # Ensure it's not deleted


class TravellogLogTestCase(TestCase):
    client_class = APIClient

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('test', 'password', picture='')
        cls.other_user = User.objects.create_user('test2', 'password', picture='')
        cls.trip = Trip.objects.create(writer=cls.user, name='My First Trip')
        cls.trip_log_url = reverse('trips-log-list', kwargs={'parent_lookup_trip': cls.trip.pk})
        cls.log = Log.objects.create(trip=cls.trip, description='제주도 도착', location='ChIJxb_FCjv7DDURZ8wL9kMz_-0',
                                     location_name='제주국제공항', visited_at=datetime.now())
        cls.user_credential = {'uid': 'test', 'password': 'password'}
        cls.other_user_credential = {'uid': 'test2', 'password': 'password'}

    # Create
    def test_create_by_owner(self):
        c = self.client
        c.login(**self.user_credential)
        resp = c.post(self.trip_log_url, {'description': '설명',
                                          'location': 'ChIJxb_FCjv7DDURZ8wL9kMz_-0',
                                          'location_name': '제주국제공항',
                                          'visited_at': '2017-09-30T16:01:00Z'})
        self.assertEqual(201, resp.status_code, 'Response code should be 201')

    def test_create_by_others(self):
        c = self.client
        c.login(**self.other_user_credential)
        resp = c.post(self.trip_log_url, {'description': '설명',
                                          'location': 'ChIJxb_FCjv7DDURZ8wL9kMz_-0',
                                          'location_name': '제주국제공항',
                                          'visited_at': '2017-09-30T16:01:00Z'})
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')

    def test_create_by_unauthorized(self):
        c = self.client
        resp = c.post(self.trip_log_url, {'description': '설명',
                                          'location': 'ChIJxb_FCjv7DDURZ8wL9kMz_-0',
                                          'location_name': '제주국제공항',
                                          'visited_at': '2017-09-30T16:01:00Z'})
        self.assertTrue(401 <= resp.status_code <= 403, 'Response code should be between 401 and 403')
