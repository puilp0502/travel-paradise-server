from django.conf import settings
from django.db.models.signals import post_save, pre_delete, m2m_changed
from django.dispatch import receiver
from elasticsearch_dsl import Text, Date, Boolean, Nested
from elasticsearch_dsl import Index, DocType, analyzer, tokenizer, InnerObjectWrapper
from elasticsearch_dsl.connections import connections

from .models import Trip as TripModel, Log as LogModel


class Log(InnerObjectWrapper):
    pass


class Trip(DocType):
    class Meta:
        index = 'trip'

    title = Text(analyzer='korean')
    created_at = Date()
    is_visible = Boolean()
    logs = Nested(
        doc_class=Log,
        properties={
            'content': Text(analyzer='korean'),
            'location_name': Text(analyzer='korean'),
        }
    )

    def add_log(self, log):
        self.logs.append({'content': log.description, 'location_name': log.location_name})

    @classmethod
    def from_model(cls, trip):
        doc = cls(title=trip.name, is_visible=trip.is_visible)
        for log in trip.logs.all():
            doc.add_log(log)
        doc.meta.id = trip.pk
        return doc

    def update_from_model(self, trip):
        # Apparently updating nested object is not possible, so we don't bother updating logs.
        self.title = trip.name
        self.is_visible = trip.is_visible
        self.meta.id = trip.pk
        return self


def handle_trip_save(sender, **kwargs):
    instance = kwargs['instance']
    created = kwargs['created']

    if created:
        Trip.from_model(instance).save()
    else:
        Trip.get(id=instance.pk).update_from_model(instance).save()


def handle_log_save(sender, **kwargs):
    # Just reindex the whole document
    trip = kwargs['instance'].trip
    Trip.get(id=trip.pk).delete()
    Trip.from_model(trip).save()


def handle_trip_delete(sender, **kwargs):
    instance = kwargs['instance']
    Trip.get(id=instance.pk).delete()


if settings.QUERY_SEARCH_ENABLED:
    connections.create_connection(hosts=[getattr(settings, 'ELASTICSEARCH_HOST', '127.0.0.1')], timeout=20)

    korean_analyzer = analyzer('korean',
                               tokenizer='mecab_ko_standard_tokenizer')
    trips = Index('trip')
    trips.analyzer(korean_analyzer)

    trips.doc_type(Trip)
    post_save.connect(handle_trip_save, sender=TripModel, dispatch_uid='travellog_search_trip_save')
    post_save.connect(handle_log_save, sender=LogModel, dispatch_uid='travellog_search_log_save')
    pre_delete.connect(handle_trip_delete, sender=TripModel, dispatch_uid='travellog_search_delete')
else:
    print('ES is not enabled; not loading')
