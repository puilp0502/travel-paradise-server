from django.core.management.base import BaseCommand, CommandError
import elasticsearch
from travellog.models import Trip
from travellog.search import Trip as TripDoc, trips as index


class Command(BaseCommand):
    help = 'Recreates index and populate with model data'

    def handle(self, *args, **options):
        try:
            index.delete()
        except elasticsearch.exceptions.NotFoundError:
            pass
        index.create()
        for trip in Trip.objects.all():
            doc = TripDoc.from_model(trip)
            doc.save()
