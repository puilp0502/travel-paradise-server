from travelheaven.permissions import IsOwnerOrReadonlyPermissionMixin
from rest_framework import permissions, exceptions
from .models import (
    Trip,
    Log,
)


class IsTripOwnerOrReadOnly(IsOwnerOrReadonlyPermissionMixin):
    owner_field = 'writer'


class IsLogOwnerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            try:
                trip = Trip.objects.get(pk=view.kwargs.get('parent_lookup_trip'))
                return trip.writer == request.user
            except Trip.DoesNotExist:
                return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return obj.get_writer() == request.user


class IsPhotoOwnerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            try:
                log = Log.objects.get(pk=view.kwargs.get('parent_lookup_log'))
                return log.get_writer() == request.user
            except Log.DoesNotExist:
                return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return obj.log.get_writer() == request.user


class IsTripVisibleOrNX(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            trip = Trip.objects.get(pk=view.kwargs.get('parent_lookup_trip'))
            if not trip.is_visible and trip.writer != request.user:
                raise exceptions.NotFound()
        except Trip.DoesNotExist:
            raise exceptions.NotFound()
        return True

    def has_object_permission(self, request, view, obj):
        try:
            trip = Trip.objects.get(pk=view.kwargs.get('parent_lookup_trip'))
            if not trip.is_visible and trip.writer != request.user:
                raise exceptions.NotFound()
        except Trip.DoesNotExist:
            raise exceptions.NotFound()
        return True


class IsItselfVisibleOrNX(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if not obj.is_visible and obj.writer != request.user:
            raise exceptions.NotFound()
        return True
