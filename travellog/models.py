from django.db import models


class Trip(models.Model):
    name = models.CharField(max_length=140, verbose_name='Name')
    writer = models.ForeignKey('accounts.User', on_delete=models.CASCADE)
    is_visible = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    tags = models.ManyToManyField('Tag', blank=True)
    liked_by = models.ManyToManyField('accounts.User', related_name='liked_trips')

    def __str__(self):
        return '<Trip %s by %s>' % (self.name, self.writer)


class Log(models.Model):
    trip = models.ForeignKey(Trip, related_name='logs')
    description = models.TextField()
    location = models.CharField(max_length=64, help_text='Google Places ID')
    location_name = models.CharField(max_length=64, help_text='Backup location name', blank=True)
    location_address = models.CharField(max_length=140, help_text='Backup location address', blank=True)
    visited_at = models.DateTimeField(null=True, blank=True)  # if null, not visited

    def get_writer(self):
        return self.trip.writer

    def __str__(self):
        return '<Log @ %s in Trip %s>' % (self.location_name, self.trip.name)


class Picture(models.Model):
    log = models.ForeignKey('Log', related_name='pictures')
    picture = models.ImageField()


class Tag(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def __str__(self):
        return "<Tag %s>" % self.name


class RecommendedTagPlan(models.Model):
    tag = models.ForeignKey(Tag)
    background = models.ImageField()
    start = models.DateTimeField()
    end = models.DateTimeField()

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "<Tag Recommendation: %s (from %s to %s)>" % (self.tag.name, self.start, self.end)
