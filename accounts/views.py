from io import BytesIO

import requests
from django.conf import settings
from rest_framework import exceptions
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotAuthenticated
from rest_framework import status
from rest_framework.authtoken.models import Token
from jwt.exceptions import InvalidTokenError

from travelheaven.pagination import TripPagination
from travellog.models import Trip
from .models import User
from .serializers import UserSerializer
from .permissions import UserAccessPermission
from .utils import validate_firebase_token

from travellog.serializers import TripSerializer


class UserRegister(APIView):
    permission_classes = (AllowAny,)
    """
    Issues an API token based on Firebase token. Creates user if necessary.

    [Parameters]
    token: A Firebase token acquired from client.

    [Return]
    token: A token that can be used to authenticate against API endpoints.
    """
    def post(self, request):
        try:
            token = request.data['token']
            claims = validate_firebase_token(token, 'travel-heaven')
        except InvalidTokenError as e:
            if settings.DEBUG:
                return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({'detail': 'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)
        except KeyError:
            return Response({'detail': '`token` should not be null.'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(uid=claims['user_id'])
        except User.DoesNotExist:
            user = User.objects.create_firebase_user(claims['user_id'],
                                                     email=claims.get('email'),
                                                     display_name=claims.get('name'),
                                                     )
            try:
                picture_url = claims.get('picture')
                name = picture_url.split('/')[-1]
                resp = requests.get(picture_url, timeout=3)
                resp.raise_for_status()
                user.picture.save(name, BytesIO(resp.content))
            except (requests.exceptions.HTTPError, AttributeError):
                pass

        if user.is_active:
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key}, status=status.HTTP_201_CREATED if created else status.HTTP_200_OK)
        else:
            return Response({'detail': 'Login not allowed'}, status=status.HTTP_403_FORBIDDEN)


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    lookup_field = 'uid'
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (UserAccessPermission,)

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        if self.kwargs[lookup_url_kwarg] == 'me':
            try:
                self.kwargs[lookup_url_kwarg] = self.request.user.uid
            except AttributeError:  # AnonymousUser
                raise NotAuthenticated()

        return super().get_object()


class UserPost(generics.ListAPIView):
    pagination_class = TripPagination
    serializer_class = TripSerializer

    def get_queryset(self):
        uid = self.kwargs.get('uid')
        if uid is None:
            raise exceptions.ParseError(detail='Please provide a username.')
        if uid == "me":
            if not self.request.user.is_authenticated:
                raise NotAuthenticated()
            uid = self.request.user.uid
        qs = Trip.objects.filter(writer__uid=uid, is_visible=True).order_by('-created_at')
        return qs


@api_view()
def user_likes(request, uid):
    if uid == 'me':
        if not request.user.is_authenticated:
            raise NotAuthenticated()
        user = request.user
    else:
        user = User.objects.get(uid=uid)
    queryset = user.liked_trips.all()
    paginator = LimitOffsetPagination()
    result = paginator.paginate_queryset(queryset, request)
    liked_trips = TripSerializer(result, many=True, context={'request': request})
    paginated_response = paginator.get_paginated_response(liked_trips.data)
    return paginated_response


@api_view()
def feed(request):
    if not request.user.is_authenticated:
        raise NotAuthenticated()
    user = request.user
    queryset = Trip.objects.filter(tags__in=user.interest.all()).order_by('-created_at')
    paginator = LimitOffsetPagination()
    result = paginator.paginate_queryset(queryset, request)
    trips = TripSerializer(result, many=True, context={'request': request})
    paginated_response = paginator.get_paginated_response(trips.data)
    return paginated_response
