from django.conf.urls import url, include
from .views import UserRegister, UserDetail, UserPost, user_likes

urlpatterns = [
    url(r'^$', UserRegister.as_view()),
    url(r'^(?P<uid>\w+)/$', UserDetail.as_view()),
    url(r'^(?P<uid>\w+)/posts/$', UserPost.as_view()),
    url(r'^(?P<uid>\w+)/likes/$', user_likes)
]
