from rest_framework import serializers

from travellog.models import Tag
from .models import User


class UserSerializer(serializers.ModelSerializer):
    interest = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Tag.objects
    )

    class Meta:
        model = User
        fields = ('uid', 'display_name', 'picture', 'interest')
        read_only_fields = ('uid', )
