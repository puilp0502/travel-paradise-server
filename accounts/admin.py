from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User


class UserAdmin(BaseUserAdmin):
    ordering = ('uid', 'email', 'display_name',)
    list_display = ('uid', 'email', 'display_name',)
    fieldsets = (
        (None, {'fields': ('uid', 'password')}),
        ('Personal info', {'fields': ('display_name', 'email', 'picture')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Travel Heaven', {'fields': ('interest', )})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('uid', 'password1', 'password2'),
        }),
    )
    readonly_fields = ('date_joined',)


admin.site.register(User, UserAdmin)
