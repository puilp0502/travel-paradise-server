# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-11 10:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='picture',
            field=models.ImageField(blank=True, default='profile/default.png', upload_to='profile/', verbose_name='Profile picture'),
        ),
    ]
