from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager, PermissionsMixin
from django.db import models


class FirebaseUserManager(BaseUserManager):
    def create_user(self, username, password=None, email=None, picture=None):
        """
        Create user using given parameters.
        A user created with this method can only be accessed by admin interface.
        """
        if not username:
            raise ValueError('User must have a username.')
        user = self.model(
            uid=username,
            display_name=username,
            email=self.normalize_email(email),
            picture=picture,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, uid, password, email=None, picture=''):
        user = self.create_user(uid, password, email=email, picture=picture)
        user.is_superuser = True
        user.is_staff = True
        user.save()

    def create_firebase_user(self, uid, email=None, display_name=None, picture=''):
        """
        Create a user associated with Firebase Authentication.

        """
        if not display_name:
            raise ValueError("Firebase User's profile name must not be None.")
        user = self.model(
            uid=uid,
            email=self.normalize_email(email),
            display_name=display_name,
            picture=picture,
        )
        user.set_unusable_password()  # Prevent firebase user from accessing django web interface
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    """
    Django User which is linked to Firebase Authentication
    """
    objects = FirebaseUserManager()

    # Either firebase-auth uid or plain username
    uid = models.CharField(max_length=64, unique=True, verbose_name='UID')
    picture = models.ImageField(verbose_name='Profile picture', upload_to='profile/',
                                default='profile/default.png', blank=True)
    email = models.EmailField(blank=True)
    display_name = models.CharField(max_length=32, verbose_name='Profile name')
    date_joined = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    interest = models.ManyToManyField('travellog.Tag', related_name='interested_user')
    USERNAME_FIELD = 'uid'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return self.display_name

    def get_short_name(self):
        return self.display_name.split(' ')[0]

    def __str__(self):
        return "<User %s (%s)>" % (self.display_name, self.email)
