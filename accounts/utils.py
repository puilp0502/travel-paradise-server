import base64
from datetime import timedelta, datetime
import json
import re

from cryptography.x509 import load_pem_x509_certificate, load_der_x509_certificate
from cryptography.hazmat.backends import default_backend
import jwt
from jwt.exceptions import InvalidTokenError, InvalidAudienceError, InvalidIssuerError
import requests

_CERT_CACHE = {}

ISSUER_ROOT = "https://securetoken.google.com/"


def get_google_certs():
    if "expiry" in _CERT_CACHE and "cert" in _CERT_CACHE:
        if datetime.now() < _CERT_CACHE["expiry"]:
            return _CERT_CACHE["cert"]

    response = requests.get("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com")
    max_age = re.search(r'max-age=(\d+)?,', response.headers['Cache-Control']).group(1)
    _CERT_CACHE["expiry"] = datetime.now() + timedelta(seconds=int(max_age))
    _CERT_CACHE["cert"] = response.json()
    return _CERT_CACHE["cert"]


def validate_firebase_token(jwt_token, project_id):
    header = jwt_token.split('.')[0]
    # Fix padding
    header += '=' * (len(header) % 4)
    # Parse header
    header = json.loads(base64.b64decode(header).decode('utf-8'))

    # load key id from the header
    try:
        key_id = header['kid']
    except KeyError:
        raise InvalidTokenError('\'kid\' does not exist in the header of the token. Cannot authenticate.')
    # Grab certificates from google
    google_certs = get_google_certs()
    # Load the certificate specified by 'kid'
    certificate = load_pem_x509_certificate(google_certs[key_id].encode('utf-8'), default_backend())

    issuer = ISSUER_ROOT + project_id
    try:
        claims = jwt.decode(jwt_token, key=certificate.public_key(), issuer=issuer, audience=project_id)
    except (InvalidIssuerError, InvalidAudienceError) as e:
        raise InvalidTokenError('Invalid issuer or audience; maybe wrong project id?') from e

    return claims
