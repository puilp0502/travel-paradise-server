#!/bin/bash

python manage.py migrate && \
	python manage.py reindex && \
	gunicorn -w 4 travelheaven.wsgi -b 0.0.0.0:8000
